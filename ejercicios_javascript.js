function mayor(a, b) {
  if (a > b) {
    console.log("Es mayor el: " + a);
  } else if (a < b) {
    console.log("Es mayor el: " + b);
  } else {
    console.log("Son iguales");
  }
}

function datos(int) {
  if (int % 2 == 0) {
    console.log("Es par");
  } else {
    console.log("Es impar");
  }
  if (int % 3 == 0) {
    console.log("Es divisible por 3");
  } else {
    console.log("No es divisible por 3");
  }
  if (int % 5 == 0) {
    console.log("Es divisible por 5");
  } else {
    console.log("No es divisible por 5");
  }
  if (int % 7 == 0) {
    console.log("Es divisible por 7");
  } else {
    console.log("No es divisible por 7");
  }
}

function SumaValores(arr) {
  let suma = 0;
  for (i = 0; i < arr.length; i++) {
    suma += arr[i];
  }
  console.log(suma);
}

function factorial(int) {
  let result = 1;
  for (i = 0; i < int; i++) {
    if (i != 0) {
      result = result * i;
    }
    console.log(i);
  }
  console.log(result);
}

function primo(int) {
  let NoEsPrimo = false;
  for (i = 0; i < int && !NoEsPrimo; i++) {
    if (int % i === 0 && i !== 0 && i !== 1) {
      NoEsPrimo = true;
    }
  }
  if (NoEsPrimo === true) {
    console.log("No es un numero primo");
  } else {
    console.log("Es un numero primo");
  }
}

function fibonacci(int) {
  let NumeroFibonacci = [];

  if (int === 1) {
    NumeroFibonacci[0] = 1;
  } else if (int >= 2) {
    NumeroFibonacci[0] = 1;
    NumeroFibonacci[1] = 1;
  }

  if (int >= 3) {
    for (i = 1; i < int - 1; i++) {
      if (i + 1 !== undefined) {
        let NewNum = i + 1;
        let LastNum = i - 1;
        NumeroFibonacci[NewNum] = NumeroFibonacci[i] + NumeroFibonacci[LastNum];
      }
    }
  }
  console.log(NumeroFibonacci);
}

function primocifras(int) {
  let Number = 1;
  let NoEsPrimo = false;
  if (int > 1) {
    for (i = 0; i < int - 1; i++) {
      Number *= 10;
    }
  }
  while (!NoEsPrimo) {
    for (i = 2; i < Number && !NoEsPrimo; i++) {
      if (Number % i === 0) {
        NoEsPrimo = true;
      }
    }
    if (NoEsPrimo === true) {
      Number++;
      NoEsPrimo = false;
    } else if (NoEsPrimo === false) {
      NoEsPrimo = true;
    }
  }
  console.log(Number);
}

function capitaliza(string) {
  string = string.toLowerCase();
  console.log(string.charAt(0).toUpperCase() + string.substring(1));
}

function palabra(string) {
  string = string.toLowerCase();
  let Largo = string.length;
  let NumConsonantes = 0;
  let NumVocales = 0;
  for (i = 0; i < Largo; i++) {
    switch (string[i]) {
      case "a":
      case "e":
      case "i":
      case "o":
      case "u":
        NumVocales += 1;
        break;
      default:
        NumConsonantes += 1;
        break;
    }
  }
  console.log("El numero de letras que tiene es: " + Largo);
  if (Largo % 2 === 0) {
    console.log("El largo es par");
  } else {
    console.log("El largo es impar");
  }
  console.log("Vocales: " + NumVocales);
  console.log("Consonantes: " + NumConsonantes);
}

function hoy() {
  let DiasSemana = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];
  let DiaDeHoy = new Date();
  console.log("Hoy es " + DiasSemana[DiaDeHoy.getDay()-1]);
}

function Navidad() {
  let Today = new Date();
  let Christmas = new Date(Today.getFullYear(),11,25);
  let DaysBetweenDates = parseInt((Christmas - Today) / (1000 * 60 * 60 * 24));
  DaysBetweenDates++;
  console.log(DaysBetweenDates);
}

function analiza(array) {
  let Suma = 0;
  let Menor = 0;
  let Mayor = 0;
  for (i = 0; i < array.length; i++) {
    if (i === 1) {
      Mayor = i;
      Menor = i;
    }
    if (Mayor < i) {
      Mayor = i;
    }
    if (Menor < i) {
      Menor = i;
    }
    Suma = Suma + array[i];
  }
  console.log("La suma de los " + array.length + " es " + Suma);
  console.log("El numero mayor es " + Mayor);
  console.log("El numero menor es " + Menor);
}

// noprotect
function random() {
  let intento = 0;
  let UserAnswer = 0;
  const Min = 1;
  const Max = 100;
  let RandomNumber = (
    Math.floor(Math.random() * (Max + 1 - Min)) + Min
  ).toString();
  while (UserAnswer != RandomNumber) {
    Intento++;
    UserAnswer = prompt("Please enter a number(1-100)", "1");
    if (UserAnswer < Min) {
      console.log(
        "El numero que has introducido es menor que el minimo que es " + Min
      );
    } else if (UserAnswer > Max + 1) {
      console.log(
        "El numero que has introducido es mayor que el maximo que es " + Max
      );
    } else if (UserAnswer == RandomNumber) {
      console.log("Numero Correcto en " + Intentos + " intentos");
    } else if (UserAnswer > RandomNumber) {
      console.log("Demasiado Alto");
    } else if (UserAnswer < RandomNumber) {
      console.log("Demasiado Bajo");
    }
  }
}